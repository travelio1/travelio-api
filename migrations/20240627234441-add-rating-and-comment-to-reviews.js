'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('reviews', 'comment', {
      type: Sequelize.STRING,
      allowNull: true, // Allow null values for comments
    });
  },

  async down(queryInterface) {
    await queryInterface.removeColumn('reviews', 'comment');
  },
};
