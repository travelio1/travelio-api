'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('reviews', 'rating', {
      type: Sequelize.INTEGER,
      allowNull: true, // Temporarily allow null values
    });

    await queryInterface.sequelize.query(
      'UPDATE reviews SET rating = 0 WHERE rating IS NULL',
    ); // Set default value

    await queryInterface.changeColumn('reviews', 'rating', {
      type: Sequelize.INTEGER,
      allowNull: false,
    });

    await queryInterface.removeColumn('reviews', 'component');
    await queryInterface.removeColumn('reviews', 'value');
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.addColumn('reviews', 'component', {
      type: Sequelize.STRING,
      allowNull: false,
    });
    await queryInterface.addColumn('reviews', 'value', {
      type: Sequelize.INTEGER,
      allowNull: false,
    });
    await queryInterface.removeColumn('reviews', 'rating');
  },
};
