import { Sequelize } from 'sequelize';
const env: string = process.env.NODE_ENV || 'development';
import config from './config.json'; // Adjust the path as necessary

const sequelize = new Sequelize(
  config[env].database,
  config[env].username,
  config[env].password,
  {
    host: config[env].host,
    dialect: config[env].dialect,
  },
);

export default sequelize;
