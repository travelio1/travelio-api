import { Injectable } from '@nestjs/common';
import User from './models/user.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class UsersService {
  async findOne(email: string): Promise<{
    id: string;
    email: string;
    name: string;
    password_hash: string;
  } | null> {
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return null;
    }
    return {
      id: user.id,
      email: user.email,
      name: user.firstName,
      password_hash: user.passwordHash,
    };
  }

  async create(name: string, email: string, hashedPassword: string) {
    try {
      const newUser = await User.create({
        id: this.generateId(),
        firstName: name,
        email: email,
        passwordHash: hashedPassword,
      });
      return newUser;
    } catch (error) {
      console.error('Error during create:', error);
      return null;
    }
  }

  private generateId(): string {
    return uuidv4();
  }
}
