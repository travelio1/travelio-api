import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/sequelize';
import { firstValueFrom } from 'rxjs';
import { Place } from './place.model';
import { Op } from 'sequelize';
import { Review } from 'src/reviews/review.model';

@Injectable()
export class PlacesService {
  private readonly logger = new Logger(PlacesService.name);

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    @InjectModel(Place)
    private placeModel: typeof Place,
    @InjectModel(Review)
    private reviewModel: typeof Review,
  ) {}

  async findNearbyPlaces(
    userCoordinates: { lat: number; lon: number },
    radius: number,
    categories: string[],
  ): Promise<any[]> {
    const city = await this.getCityFromCoordinates(userCoordinates);

    const places = await this.placeModel.findAll({
      where: {
        category: {
          [Op.in]: categories,
        },
        city: city,
      },
      include: [
        {
          model: Review,
        },
      ],
    });

    // Calculate average rating for each place
    const placeIds = places.map((place) => place.id);
    const ratings = await this.reviewModel.findAll({
      attributes: [
        'placeId',
        [
          this.reviewModel.sequelize.fn(
            'AVG',
            this.reviewModel.sequelize.col('rating'),
          ),
          'averageRating',
        ],
      ],
      where: {
        placeId: {
          [Op.in]: placeIds,
        },
      },
      group: ['placeId'],
    });

    const ratingsMap = ratings.reduce((acc, rating) => {
      acc[rating.placeId] = rating.get('averageRating');
      return acc;
    }, {});

    const placesWithAverageRating = places.map((place) => {
      return {
        ...place.toJSON(),
        averageRating: ratingsMap[place.id] || 0,
      };
    });

    return placesWithAverageRating;
  }

  async getPlaceDetails(id: string): Promise<any> {
    const place = await this.placeModel.findByPk(id, {
      include: [
        {
          model: Review,
        },
      ],
    });

    if (!place) {
      throw new HttpException('Place not found', HttpStatus.NOT_FOUND);
    }

    const ratings = await this.reviewModel.findAll({
      attributes: [
        [
          this.reviewModel.sequelize.fn(
            'AVG',
            this.reviewModel.sequelize.col('rating'),
          ),
          'averageRating',
        ],
      ],
      where: {
        placeId: place.id,
      },
    });

    return {
      ...place.toJSON(),
      averageRating: ratings[0].get('averageRating') || 0,
    };
  }

  async createReview(
    placeId: number,
    { comment, rating }: { comment: string; rating: number },
  ): Promise<any> {
    const place = await this.placeModel.findByPk(placeId);

    if (!place) {
      throw new HttpException('Place not found', HttpStatus.NOT_FOUND);
    }

    const review = await this.reviewModel.create({
      placeId,
      comment,
      rating,
    });

    return review;
  }

  // async findNearbyPlaces(
  //   userCoordinates: { lat: number; lon: number },
  //   radius: number,
  //   categories: number[],
  // ): Promise<any> {
  //   const validRadius = Math.max(1, Math.min(2000, radius));
  //   const validCategories = categories.slice(0, 5); // Limit to 5 categories

  //   console.log('validCategories', validCategories);

  //   const body = {
  //     request: 'pois',
  //     geometry: {
  //       geojson: {
  //         type: 'Point',
  //         coordinates: [userCoordinates.lon, userCoordinates.lat],
  //       },
  //       buffer: validRadius,
  //     },
  //     filters: {
  //       category_ids: validCategories,
  //     },
  //   };

  //   const headers = {
  //     Authorization: `${this.configService.get<string>('OPENROUTESERVICE_API_KEY')}`,
  //     'Content-Type': 'application/json',
  //   };

  //   try {
  //     const response = await firstValueFrom(
  //       this.httpService
  //         .post('https://api.openrouteservice.org/pois', body, {
  //           headers,
  //         })
  //         .pipe(
  //           retry(3), // Retry the request up to 3 times
  //           catchError((error) => {
  //             console.error(
  //               'Error during nearby places retrieval:',
  //               error.response?.status,
  //               error.response?.data,
  //             );
  //             throw new HttpException(
  //               'Failed to retrieve nearby places',
  //               HttpStatus.INTERNAL_SERVER_ERROR,
  //             );
  //           }),
  //         ),
  //     );

  //     return this.sortFeaturesByDistance(response.data.features).map(
  //       (feature) => ({
  //         name: feature.properties.name,
  //         location: feature.properties.address,
  //         rating: feature.properties.rating || 0,
  //         imageUrl: 'https://www.example.com/default-image.jpg', // Replace with actual image URL if available
  //       }),
  //     );
  //   } catch (error) {
  //     throw new HttpException(
  //       'Failed to retrieve nearby places',
  //       HttpStatus.INTERNAL_SERVER_ERROR,
  //     );
  //   }
  // }

  sortFeaturesByDistance(features: any[]): any[] {
    return features.sort(
      (a, b) => a.properties.distance - b.properties.distance,
    );
  }

  async getCityFromCoordinates(userCoordinates: {
    lat: number;
    lon: number;
  }): Promise<string> {
    this.logger.debug(`userCoordinates: ${JSON.stringify(userCoordinates)}`);

    const apiKey = this.configService.get<string>('OPENROUTESERVICE_API_KEY');
    const params = new URLSearchParams({
      api_key: apiKey,
      'point.lon': userCoordinates.lon.toString(),
      'point.lat': userCoordinates.lat.toString(),
      size: '1',
      layers: 'locality', // Ensure we are fetching the locality layer for city/town names
    });

    const url = `https://api.openrouteservice.org/geocode/reverse?${params.toString()}`;

    try {
      const response = await firstValueFrom(this.httpService.get(url));
      if (response.data.features.length === 0) {
        throw new Error('No features found in response');
      }

      const city =
        response.data.features[0].properties.locality ||
        response.data.features[0].properties.city ||
        response.data.features[0].properties.town ||
        response.data.features[0].properties.village;

      if (!city) {
        throw new Error('City not found in response');
      }

      return city;
    } catch (error) {
      this.logger.error(
        'Error during city retrieval:',
        error.response?.status,
        error.response?.data,
      );
      throw new HttpException(
        'Failed to retrieve city from coordinates',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
