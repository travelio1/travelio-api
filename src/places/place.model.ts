import { Column, DataType, Model, Table, HasMany } from 'sequelize-typescript';
import { Review } from 'src/reviews/review.model';

@Table({ tableName: 'places', timestamps: false })
export class Place extends Model<Place> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  description: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  category: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  imageUrl: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  city: string;

  @HasMany(() => Review)
  reviews: Review[];
}
