import { Module } from '@nestjs/common';
import { PlacesController } from './places.controller';
import { PlacesService } from './places.service';
import { HttpModule } from '@nestjs/axios';
import { SequelizeModule } from '@nestjs/sequelize';
import { Place } from './place.model';
import { Review } from 'src/reviews/review.model';

@Module({
  imports: [HttpModule, SequelizeModule.forFeature([Place, Review])],
  controllers: [PlacesController],
  providers: [PlacesService],
})
export class PlacesModule {}
