import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { PlacesService } from './places.service';
import { GetNearbySearchDTO } from './dtos/getNearbySearch.dto';
import { Response } from 'express';
import { CreateReviewDto } from './dtos/create-review.dto';
import { GetCityFromCoordinatesDTO } from './dtos/get-city-from-coordinates.dto';
@Controller('places')
export class PlacesController {
  private logger = new Logger(PlacesController.name);

  constructor(private readonly placesService: PlacesService) {}

  @Get(':id')
  async getPlaceDetails(@Param('id') id: string, @Res() res: Response) {
    try {
      const placeDetails = await this.placesService.getPlaceDetails(id);

      return res.status(HttpStatus.OK).send(placeDetails);
    } catch (error) {
      this.logger.error(
        `Failed to retrieve place details: ${error.message}`,
        error.stack,
      );
      throw new HttpException(
        'Failed to retrieve place details',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Post()
  async getPlaces(
    @Body() nearbySearch: GetNearbySearchDTO,
    @Res() res: Response,
  ) {
    try {
      const nearbyPlaces = await this.placesService.findNearbyPlaces(
        nearbySearch.userCoordinates,
        nearbySearch.radius,
        nearbySearch.categories,
      );

      return res.status(HttpStatus.OK).send(nearbyPlaces);
    } catch (error) {
      this.logger.error(
        `Failed to retrieve places: ${error.message}`,
        error.stack,
      );
      throw new HttpException(
        'Failed to retrieve places',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Post('user/city')
  async getCityFromCoordinates(
    @Body() coordinatesDto: GetCityFromCoordinatesDTO,
    @Res() res: Response,
  ) {
    try {
      const city =
        await this.placesService.getCityFromCoordinates(coordinatesDto);

      return res.status(HttpStatus.OK).send({ city });
    } catch (error) {
      this.logger.error(
        `Failed to retrieve city: ${error.message}`,
        error.stack,
      );
      throw new HttpException(
        'Failed to retrieve city from coordinates',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Post(':id/reviews')
  async createReview(
    @Param('id') id: number,
    @Body() review: CreateReviewDto,
    @Res() res: Response,
  ) {
    try {
      const newReview = await this.placesService.createReview(id, review);
      return res.status(HttpStatus.CREATED).send(newReview);
    } catch (error) {
      this.logger.error(
        `Failed to create review: ${error.message}`,
        error.stack,
      );
      throw new HttpException(
        'Failed to create review',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
