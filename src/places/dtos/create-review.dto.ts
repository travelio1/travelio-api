export class CreateReviewDto {
  comment: string;
  rating: number;
}
