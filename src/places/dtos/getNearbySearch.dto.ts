import { Type } from 'class-transformer';
import {
  IsInt,
  IsNumber,
  IsObject,
  Max,
  Min,
  ValidateNested,
  IsArray,
  IsString,
} from 'class-validator';

class CoordinatesDTO {
  @IsNumber()
  lat: number;

  @IsNumber()
  lon: number;
}

export class GetNearbySearchDTO {
  @IsObject()
  @ValidateNested()
  @Type(() => CoordinatesDTO)
  userCoordinates: CoordinatesDTO;

  @IsInt()
  @Min(1)
  @Max(2000)
  radius: number;

  @IsArray()
  @IsString({ each: true })
  categories: string[];
}
