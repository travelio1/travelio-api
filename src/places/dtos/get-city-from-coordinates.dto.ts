import { IsNotEmpty, IsNumber } from 'class-validator';

export class GetCityFromCoordinatesDTO {
  @IsNotEmpty()
  @IsNumber()
  lat: number;

  @IsNotEmpty()
  @IsNumber()
  lon: number;
}
