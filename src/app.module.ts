import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { PlacesModule } from './places/places.module';
import { Place } from './places/place.model';
import { Review } from './reviews/review.model';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const sequelizeConfig = require('../config/config');

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: false,
      envFilePath: '.env',
    }),

    SequelizeModule.forRoot({
      ...sequelizeConfig[process.env.NODE_ENV || 'development'], // Use environment specific config
      models: [Place, Review],
      autoLoadModels: true,
      synchronize: false, // should be false in production
    }),
    SequelizeModule.forFeature([Place, Review]),

    AuthModule,
    UsersModule,
    PlacesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
