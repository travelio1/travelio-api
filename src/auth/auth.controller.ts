import { Controller, Post, Body, Res, Logger, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Request, Response } from 'express';
import { LoginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  private readonly logger = new Logger(AuthController.name);

  constructor(private authService: AuthService) {}

  @Post('login')
  async login(
    @Body() loginDto: LoginDto,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const { email, password } = loginDto;
      const response = await this.authService.login(email, password);

      if (!response) {
        return res.status(401).json({ message: 'Unauthorized' });
      }

      return res.status(200).json(response);
    } catch (error) {
      this.logger.error(error.message);
      return res.status(500).json({ message: 'Internal server error' });
    }
  }

  @Post('signup')
  async signup(
    @Body() loginDto: LoginDto,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const { name, email, password } = loginDto;
      const response = await this.authService.signup(name, email, password);

      if (!response) {
        return res.status(409).json({ message: 'Conflict' });
      }

      return res.status(201).json(response);
    } catch (error) {
      this.logger.error(error.message);
      return res.status(500).json({ message: 'Internal server error' });
    }
  }
}
