import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @IsString()
  name: string;

  @IsString()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  password: string;
}
