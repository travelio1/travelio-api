import { Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private usersService: UsersService,
    private configService: ConfigService,
  ) {}

  async login(email: string, pass: string): Promise<any> {
    try {
      const user = await this.usersService.findOne(email);

      if (user && (await bcrypt.compare(pass, user.password_hash))) {
        const payload = { email: user.email, sub: user.id };
        const access_token = sign(
          payload,
          this.configService.get<string>('JWT_SECRET'),
          { expiresIn: '7d' },
        );
        return {
          user: {
            userId: user.id,
            name: user.name,
            email: user.email,
          },
          accessToken: access_token,
        };
      }
      return null;
    } catch (error) {
      this.logger.error('Error during login:', error);
      return null;
    }
  }

  async signup(name: string, email: string, pass: string): Promise<any> {
    if (!pass) {
      this.logger.error('Signup error: Password is required');
      return null;
    }

    try {
      const user = await this.usersService.findOne(email);

      if (user) {
        this.logger.error('Signup error: User already exists');
        return null;
      }

      const hashedPassword = await bcrypt.hash(pass, 10);
      const newUser = await this.usersService.create(
        name,
        email,
        hashedPassword,
      );
      const payload = { email, sub: newUser.id };

      const access_token = sign(
        payload,
        this.configService.get<string>('JWT_SECRET'),
        { expiresIn: '7d' },
      );
      return {
        user: {
          id: newUser.id,
          name: name,
          email: newUser.email,
        },
        accessToken: access_token,
      };
    } catch (error) {
      this.logger.error('Error during signup:', error);
      return null;
    }
  }
}
